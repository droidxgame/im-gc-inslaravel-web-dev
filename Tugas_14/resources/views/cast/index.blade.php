@extends('layout.master')

@section('content')
<table class="table">
    <thead class="thead-dark">
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>

            </tr>
        @empty
        <tr>
            <td>data cast Kosong</td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection